
Building Autoware.Auto for QNX
==============================

.. contents:: Table of Contents
   :depth: 2
   :local:


The following instructions go over the steps for building Autoware.Auto for QNX.

Overview of the build process
-----------------------------

The build process will cross compile Autoware.Auto's source code against a QNX SDP 7.1 installation and the necessary cross-compiled dependencies.
This will generate binaries for the following CPU architectures:

- aarch64le
- x86_64

The generated files can then be transferred to the required target and used.

System requirements
-------------------

HOST
^^^^

- Ubuntu 20.04
- QNX SDP7.1

For instructions to install QNX SDP 7.1 please follow the link:
http://www.qnx.com/developers/docs/7.1/index.html#com.qnx.doc.qnxsdp.quickstart/topic/about.html

A combined 30GB of RAM installed on system and swap space are required for the build (if multicore processing is enabled).
Please see steps here to create or increase your swap file size: https://linuxize.com/post/how-to-add-swap-space-on-ubuntu-20-04/

TARGET
^^^^^^

- A QNX supported architecture running QNX SDP 7.1

System setup
------------

Install ROS2 for QNX
^^^^^^^^^^^^^^^^^^^^^^

You will need to add the ROS 2 installation for QNX to your system in order to build Autoware.Auto for QNX.

See instructions at https://ros2-qnx-documentation.readthedocs.io/en/latest/build_from_source.html and make sure to follow steps for the correct ROS 2 distribution (foxy, rolling, etc.). 

Follow the provided steps to setup locale and install development & ROS tools in the instructions.

Install development tools
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    # The following three packages need to be built for host first in order to then cross compile Autoware.Auto for QNX
    
    # Install CASADi for host
    sudo apt update && sudo apt install -y gcc g++ gfortran git cmake liblapack-dev pkg-config coinor-libipopt-dev 
    cd ~/
    git clone -b 3.5.5 https://github.com/casadi/casadi.git 
    cd casadi
    mkdir build
    cd build
    cmake -DWITH_IPOPT=true ..  
    sudo make install -j$(nproc)
  
    
    # Install HPMPC for host
    cd ~/
    git clone -b submodule https://github.com/cho3/hpmpc.git 
    cd hpmpc 
    mkdir build
    cd build
    cmake ..
    sudo make install -j$(nproc)

    
    # Install ACADO for host
    sudo apt install -y gnuplot doxygen graphviz 
    cd ~/
    git clone -b submodule https://github.com/cho3/acado.git 
    cd acado 
    mkdir build 
    cd build
    cmake ..
    sudo make install -j$(nproc)


Get Autoware.Auto code 
----------------------

Create a workspace and clone the Autoware.Auto source code repository.

Note: this step is currently empty and will be updated once QNX code is merged with the Autoware Foundation's Gitlab repository.

Building steps
--------------

1- Clone AutowareAuto release 1.0.0 using the given commit SHA.

.. code-block:: bash

   cd ~/
   git clone https://gitlab.com/autowarefoundation/autoware.auto/AutowareAuto.git
   cd AutowareAuto
   git checkout 42d2d6976d402ed4c18d514d3690a44d76e0d274

2- From within the directory ``~/AutowareAuto``, clone the AutowareAuto source repository and then merge them with your Autoware QNX directory.

.. code-block:: bash

    cd ~/AutowareAuto
    git clone https://gitlab.com/qnx/frameworks/autowareauto/autowareauto_qnx.git /tmp/autoware 
    rsync -haz /tmp/autoware/* . 
    rm -rf /tmp/autoware/  

3- Patch AutowareAuto's source code using the given patches

.. code-block:: bash

    git apply --ignore-whitespace AutowareAuto-release1.0.0.patch
    cp acado.patch src/tools/ament_acado

4- From within the directory ``~/AutowareAuto``, import extra dependencies.

.. code-block:: bash
    
    mkdir -p src/qnx_deps
    vcs import src/qnx_deps < qnx_deps.repos 
    mkdir -p src/external
    vcs import src/external < external.repos 

5- Apply a patch required to lanelet2 code which was imported in the previous step

.. code-block:: bash

    cd src/external/lanelet2
    git apply ../../../lanelet2.patch
    cd -

6- Disable some optional packages from getting built by adding COLCON_IGNORE.

.. code-block:: bash

    touch src/drivers/ssc_interface/COLCON_IGNORE
    touch src/tools/visualization/COLCON_IGNORE
    touch src/drivers/socketcan/COLCON_IGNORE
    touch src/external/lanelet2/lanelet2_python/COLCON_IGNORE
    touch src/external/lanelet2/lanelet2_examples/COLCON_IGNORE 

7- Run a script to automatically embed ``<build_depend>`` in the packages that depend on the packages in ``qnx_deps``.

.. code-block:: bash

    ./patch-pkgxml.py --path=src

8- Set the CPU environment variable according to your target architecture.

.. code-block:: bash

  export CPU=x86_64
  # or
  export CPU=aarch64
  # or
  unset CPU # Build for all supported architectures

9- Source ``qnxsdp-env.sh`` to set environment variables necessary for building.

.. code-block:: bash

    . ~/qnx710/qnxsdp-env.sh

Optional: Add this command to the end of ``~/.bashrc`` if you would like the environment to be set every time.

10- Build AutowareAuto for QNX.

Note: If AutowareAuto needs to be rebuilt, run ``./clean.sh``

.. code-block:: bash

    ./build-autoware.sh
